import URI from 'urijs';
import base64url from 'base64url';

export const pathToSiteURL = (
  path,
  baseUrl = process.env.BASE_URL,
) => new URI(baseUrl).path(path).href();

export const sortByDate = (items, prop = 'date') => items
  .slice() // copy
  .sort((a, b) => new Date(b[prop]).getTime() - new Date(a[prop]).getTime());

const statusClassMap = {
  online: 'tertiary-dark',
  unregistered: 'grey-light',
  offline: 'danger',
  error: 'danger',
  banned: 'grey-light',
  defunct: 'grey-light',
};

export const statusToClass = (status) => statusClassMap[status] || 'grey-light';

export const pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x);

const base64Regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;

export const isBase64 = (str) => base64Regex.test(str);

export const escapeBase64 = (str) => base64url.fromBase64(str);

export const hasDataInDatasets = ({ datasets }) => datasets && datasets.some(
  ({ data }) => data && data.length > 0,
);
