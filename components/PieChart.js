import { Doughnut, mixins } from 'vue-chartjs';

const { reactiveProp } = mixins;

export default {
  extends: Doughnut,
  props: ['options'],
  mixins: [reactiveProp],
  methods: {
    render() {
      const options = {
        ...this.options,
        borderWidth: '10px',
        responsive: true,
      };
      this.renderChart(this.chartData, options);
    },
  },
  mounted() {
    this.render();
  },
};
