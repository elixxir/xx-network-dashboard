import { Bar, mixins } from 'vue-chartjs';

const { reactiveProp } = mixins;

export default {
  extends: Bar,
  props: ['options'],
  mixins: [reactiveProp],
  methods: {
    render() {
      const options = {
        ...this.options,
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          display: false,
        },
      };

      this.renderChart(this.chartData, options);
    },
  },
  mounted() {
    this.render();
  },
};
