import { Line, mixins } from 'vue-chartjs';

const { reactiveProp } = mixins;

export default {
  extends: Line,
  mixins: [reactiveProp],
  props: ['options'],
  mounted() {
    const options = {
      ...this.options,
      lineTension: 0.5,
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false,
      },
    };

    this.renderChart(this.chartData, options);
  },
};
