import { mapGetters } from 'vuex';

export default {
  data: () => ({
    updateStack: [],
  }),
  methods: {
    addUpdate(update) {
      if (this.periodEnabled) {
        this.updateStack.push(update);
      } else {
        update();
      }
    },
    clearStack() {
      this.updateStack.forEach((fn) => fn());
      this.updateStack = [];
    },
  },
  computed: mapGetters({
    realtime: 'realtime/realtime',
    periodEnabled: 'realtime/periodEnabled',
    periodRemaining: 'realtime/periodRemaining',
  }),
  watch: {
    periodRemaining(value) {
      if (value === 0) {
        this.clearStack();
      }
    },
  },
};
