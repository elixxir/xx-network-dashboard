/* eslint-disable no-underscore-dangle */
import ReconnectingEventSource from 'reconnecting-eventsource';

const url = new URL('/v1/sse', process.env.API_URL);

const _ = {
  sse: null,
  interval: null,
};

export default {
  computed: {
    $sse() {
      return _.sse;
    },
  },
  methods: {
    connect() {
      _.sse = new ReconnectingEventSource(url.href);
    },
  },
  mounted() {
    this.connect();

    _.interval = setInterval(() => {
      if (_.sse && _.sse.readyState === 2) {
        this.connect();
      }
    }, 1500);
  },
  beforeDestroy() {
    if (_.sse) {
      _.sse.close();
    }
    delete _.sse;
    delete _.interval;
  },
};
