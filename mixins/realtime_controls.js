import { mapGetters } from 'vuex';

export default {
  computed: mapGetters({
    realtime: 'realtime/realtime',
    roundsSinceLastRefresh: 'realtime/roundsSinceLastRefresh',
  }),
  watch: {
    realtime(value) {
      if (value) {
        this.$nextTick(() => this.$fetch());
      }
    },
  },
};
