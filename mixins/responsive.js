export default {
  data() {
    return {
      touchCapable: false,
      window: {
        width: 0,
      },
    };
  },
  computed: {
    isTouchCapable() {
      return this.touchCapable;
    },
    isTouch() {
      return this.isMobile || this.isTablet;
    },
    isMobile() {
      let isMobile = false;
      if (process.client) {
        isMobile = this.window.width < 768;
      }
      return isMobile;
    },
    isTablet() {
      let isTablet = false;
      if (process.client) {
        isTablet = this.window.width >= 768 && this.window.width < 1024;
      }
      return isTablet;
    },
    isDesktop() {
      let isDesktop;
      if (process.client) {
        isDesktop = this.window.width >= 1024;
      }
      return isDesktop;
    },
    isWidescreen() {
      let isWidescreen;
      if (process.client) {
        isWidescreen = this.window.width >= 1216;
      }
      return isWidescreen;
    },
    isFullHd() {
      let fullHd;
      if (process.client) {
        fullHd = this.window.width >= 1440;
      }
      return fullHd;
    },
  },
  methods: {
    handleResize() {
      this.window.width = window.innerWidth;
    },
  },
  created() {
    if (process.client) {
      window.addEventListener('resize', this.handleResize);
      this.handleResize();

      document.addEventListener('touchstart', () => { this.touchCapable = true; });
    }
  },
  destroyed() {
    if (process.client) {
      window.removeEventListener('resize', this.handleResize);
      document.removeEventListener('touchstart', () => { this.touchCapable = true; });
    }
  },
};
