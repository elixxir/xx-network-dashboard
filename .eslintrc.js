module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    'airbnb-base',
    'plugin:vue/essential',
    'plugin:@intlify/vue-i18n/recommended',
  ],
  plugins: [
    'vue',
  ],
  rules: {
    '@intlify/vue-i18n/no-v-html': 'off',
    '@intlify/vue-i18n/no-missing-keys': 'error',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'context',
          'state',
          'storeState',
          'acc',
          'e',
          'ctx',
          'req',
          'request',
          'res',
          'response',
          '$scope',
          'app',
        ],
      },
    ],
  },
  settings: {
    'import/core-modules': ['vue', 'vuex', 'axios'],
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            alias: {
              '@': __dirname,
              '~': __dirname,
            },
          },
        },
      },
    },
    'vue-i18n': {
      localeDir: './i18n/*.json',
    },
  },
};
