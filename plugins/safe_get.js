import get from 'lodash.get';

export default (_, inject) => {
  inject('get', get);
};
