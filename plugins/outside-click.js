import Vue from 'vue';

Vue.directive('outside-click', {
  bind(el, { value: handler, expression }, vNode) {
    // Provided expression must evaluate to a function.
    if (typeof handler !== 'function') {
      const compName = vNode.context.name;
      let warn = `[Vue-click-outside:] provided expression '${expression}' is not a function, but has to be`;
      if (compName) { warn += `Found in component '${compName}'`; }

      console.warn(warn);
    }
    // eslint-disable-next-line no-param-reassign
    el.outsideClick = (e) => {
      if (!el.contains(e.target) && el !== e.target) {
        handler(e);
      }
    };

    // add Event Listeners
    document.addEventListener('click', el.outsideClick);
  },

  unbind(el) {
    // Remove Event Listeners
    document.removeEventListener('click', el.outsideClick);
    // eslint-disable-next-line no-param-reassign
    el.outsideClick = null;
  },
});
