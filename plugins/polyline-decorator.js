import Vue from 'vue';
import Vue2LeafletPolylinedecorator from 'vue2-leaflet-polylinedecorator';

Vue.component('l-polyline-decorator', Vue2LeafletPolylinedecorator);
