import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faGitlab, faDiscord } from '@fortawesome/free-brands-svg-icons';
import {
  faCaretDown,
  faSpinner,
  faSearch,
  faSyncAlt,
  faCaretUp,
  faAngleDown,
  faStopwatch,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
  faStopwatch,
  faDiscord,
  faGitlab,
  faCaretDown,
  faCaretUp,
  faSpinner,
  faSearch,
  faSyncAlt,
  faAngleDown,
);

Vue.component('fa-icon', FontAwesomeIcon);
