import Vue from 'vue';
import prettyMs from 'pretty-ms';
import prettyBytes from 'pretty-bytes';

const { isFinite, isNaN } = Number;

const notNull = (d) => d !== null;

const makeDateFormatter = ($moment) => (
  date,
  dateFormat = 'YYYY-MM-DD HH:mm:ss',
) => (
  notNull(date)
    ? $moment(date).utc().format(dateFormat)
    : '—');

const toFixed = (
  x,
  precision = 2,
) => (!isNaN(parseFloat(x)) ? parseFloat(x).toFixed(precision) : x);

const prettyS = (seconds) => ((isFinite(seconds) && !isNaN(seconds))
  ? prettyMs(seconds * 1000)
  : '—');

const shortHash = (hash) => (
  hash
    ? hash.slice(0, 4).concat('...', hash.slice(hash.length - 4, hash.length))
    : '');

const txToBytes = (x) => {
  const size = x * process.env.MESSAGE_SIZE;
  return !isNaN(size) ? prettyBytes(size) : '';
};

const toMils = (x) => {
  const mils = !isNaN(x) ? (x / 1000000).toFixed(2) : '';
  return `${mils}M`;
};

const toExplorerAddress = (address) => {
  const url = new URL(process.env.EXPLORER_URL);
  url.hash = `#/staking/query/${address}`;
  return url.href;
};

export default ({ app }) => {
  const { $moment } = app;
  const formatDate = makeDateFormatter($moment);

  Vue.filter('shortHash', shortHash);

  Vue.filter('prettyS', prettyS);

  Vue.filter(
    'formatDate',
    formatDate,
  );

  Vue.filter(
    'formatWeek',
    (date) => `${formatDate(date, 'MMM D')} - ${formatDate($moment(date).add(1, 'week'), 'll')}`,
  );

  Vue.filter(
    'formatMonth',
    (date) => formatDate(date, 'MMM yyyy'),
  );

  Vue.filter(
    'toFixed',
    toFixed,
  );

  Vue.filter(
    'txToBytes',
    txToBytes,
  );

  Vue.filter(
    'toMils',
    toMils,
  );

  Vue.filter(
    'toExplorerAddress',
    toExplorerAddress,
  );
};
