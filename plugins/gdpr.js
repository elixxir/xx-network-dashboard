export default async ({ app, $gtm }) => {
  const prefSet = app.$cookie.get('xx_preferences_set');

  if (prefSet && app.$cookie.get('xx_google') && !app.$cookie.get('xx_do_not_track')) {
    $gtm.init(process.env.GOOGLE_TAG_ID);
  }
};
