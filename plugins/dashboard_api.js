import URI from 'urijs';

const version = '/v1';

export default ({ $axios }, inject) => {
  const api = $axios.create({
    baseURL: new URI(process.env.API_URL).path(version).href(),
    validateStatus: (status) => status >= 200 && status < 500,
  });

  const dashboard = {
    getNetwork: () => api.$get('/network'),
    getRounds: (args) => api.$get('/rounds', { params: args }),
    findRound: (id) => api.$get(`/rounds/${id}`),
    findRoundIdsNear: (id) => api.$get(`/rounds/near/${id}`),
    getNodes: () => api.$get('/nodes'),
    findNode: (id) => api.$get(`/nodes/${id}`),
    search: (q) => api.$get('/search', { params: { q } }),
  };

  inject('dashboard', dashboard);
};
