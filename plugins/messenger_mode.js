export default ({ query }, inject) => {
  const messengerMode = query.xxmessenger || false;

  if (messengerMode) {
    import('@/assets/scss/messenger-mode.scss');
  }

  inject('messengerMode', messengerMode);
};
