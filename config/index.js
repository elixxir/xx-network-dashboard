const defaultEnv = {
  API_URL: process.env.API_URL || 'https://dashboard-api.xx.network',
  EXPLORER_URL: process.env.EXPLORER_URL || 'https://explorer.xx.network/',
  MESSAGE_SIZE: 1024,
  GOOGLE_TAG_ID: 'GTM-TJ4DPG8',
};

export const environments = {
  production: {
    BASE_URL: 'https://dashboard.xx.network',
    XX_NETWORK_URL: 'https://xx.network',
  },
  staging: {
    BASE_URL: 'https://staging-dashboard.xx.network',
    XX_NETWORK_URL: 'https://staging.xx.network',
  },
  development: {
    API_URL: 'https://dashboard-api.xx.network',
    BASE_URL: 'http://localhost:3000',
    XX_NETWORK_URL: 'https://staging.xx.network',
  },
};

const assigned = Object.assign(
  defaultEnv,
  environments[process.env.NODE_ENV] || environments.development,
);

Object.assign(process.env, assigned);

export const env = assigned;
