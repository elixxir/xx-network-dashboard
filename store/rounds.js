import uniqBy from 'lodash.uniqby';
import { pipe, sortByDate } from '@/utils';

export const state = () => ({
  rounds: [],
  successfulRoundsTotal: 0,
});

// types
const ADD_ROUNDS = 'ADD_ROUNDS';
const SET_SUCCESSFUL_TOTAL = 'SET_SUCCESSFUL_TOTAL';

export const getters = {
  all: ({ rounds: r }) => r,
  successfulRoundsTotal: ({ successfulRoundsTotal: t }) => t,
  latest: ({ rounds: r }) => r && r[0],
};

export const mutations = {
  [ADD_ROUNDS](storeState, rounds = []) {
    storeState.rounds = pipe(
      (r) => r.map((round) => Object.freeze(round)),
      (r) => uniqBy(r, ({ id }) => id),
      (r) => sortByDate(r, 'realtimeEnd'),
    )(rounds.concat(storeState.rounds));
  },
  [SET_SUCCESSFUL_TOTAL](storeState, newTotal) {
    storeState.successfulRoundsTotal = newTotal;
  },
};

export const actions = {
  async fetch({ commit }, { page, limit } = { page: 1, limit: 100 }) {
    const { rounds } = await this.$dashboard.getRounds({ page, limit });

    commit(ADD_ROUNDS, rounds);
  },
};
