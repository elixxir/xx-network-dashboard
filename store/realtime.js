import Vue from 'vue';

export const state = () => ({
  realtime: true,
  roundsSinceLastRefresh: 0,
  loading: false,
  realtimePeriod: 0,
  remaining: 0,
});

const RESET_ROUND_COUNT = 'RESET_ROUND_COUNT';
const ADD_ROUNDS = 'ADD_ROUNDS';
const TOGGLE_REALTIME = 'TOGGLE_REALTIME';
const SET_REALTIME_PERIOD = 'SET_REALTIME_PERIOD';
const SET = 'SET';
const REMOVE_SECOND = 'REMOVE_SECOND';
const RESET_REMAINING = 'RESET_REMAINING';

export const getters = {
  realtime: ({ realtime: r }) => r,
  roundsSinceLastRefresh: ({ roundsSinceLastRefresh: r }) => r,
  loading: ({ loading: l }) => l,
  period: ({ realtimePeriod }) => realtimePeriod,
  periodRemaining: ({ remaining }) => remaining,
  periodEnabled: ({ realtimePeriod, realtime }) => realtime && realtimePeriod !== 0,
  periodOptions: () => [
    0,
    5,
    10,
    30,
    60,
  ],
};

export const mutations = {
  [ADD_ROUNDS](storeState, num) {
    if (!storeState.realtime) {
      storeState.roundsSinceLastRefresh += num;
    }
  },
  [RESET_ROUND_COUNT](storeState) {
    storeState.roundsSinceLastRefresh = 0;
  },
  [TOGGLE_REALTIME](storeState) {
    Vue.set(storeState, 'realtime', !storeState.realtime);
  },
  [SET](storeState, realtimeState) {
    Vue.set(storeState, 'realtime', realtimeState);
  },
  [SET_REALTIME_PERIOD](storeState, period) {
    storeState.countdownEnabled = period === 0;
    storeState.realtimePeriod = period;
    storeState.remaining = period;
  },
  [REMOVE_SECOND](storeState) {
    const p = storeState.remaining;
    storeState.remaining = p !== 0 ? p - 1 : p;
  },
  [RESET_REMAINING](storeState) {
    storeState.remaining = storeState.realtimePeriod;
  },
};

let interval;
export const actions = {
  TOGGLE({ commit, state: storeState }) {
    commit(TOGGLE_REALTIME);
    if (storeState.realtime) {
      commit(RESET_ROUND_COUNT);
    }
  },
  COUNTDOWN({ commit, state: storeState }) {
    if (interval) {
      return;
    }
    interval = setInterval(() => {
      if (storeState.remaining > 0) {
        commit(REMOVE_SECOND);
      } else {
        commit(RESET_REMAINING);
      }
    }, 1000);
  },
};
