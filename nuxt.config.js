import locales from './locales.json';
import { env } from './config';

export default {
  env,
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'Quick View',
    titleTemplate: '%s - xx dashboard',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      {
        hid: 'og:image',
        property: 'og:image',
        content: new URL('/icon.png', env.BASE_URL),
      },
      {
        hid: 'robots',
        name: 'robots',
        content: process.env.NODE_ENV === 'production' ? 'all' : 'none',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        src: '//polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
        body: true,
      },
    ],
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/scss/app.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '~plugins/polyline-decorator.js', mode: 'client' },
    '~plugins/messenger_mode.js',
    '~plugins/gdpr.js',
    '~plugins/vue-page-title.js',
    '~plugins/dashboard_api.js',
    '~plugins/filters.js',
    '~plugins/safe_get.js',
    '~plugins/icons.js',
    '~plugins/outside-click.js',
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment',
  ],

  /*
  ** Global CSS
  */
  styleResources: {
    scss: [
      '~/assets/scss/variables.scss',
      '~/assets/scss/mixins.scss',
      '~/assets/scss/spacing.scss',
    ],
  },

  /*
  ** Nuxt.js modules
  */
  modules: [
    ['cookie-universal-nuxt', { alias: 'cookie' }],
    '@nuxtjs/gtm',
    '@nuxtjs/sentry',
    '@nuxtjs/bulma',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    'nuxt-leaflet',
    'nuxt-rfg-icon',
    ['nuxt-i18n', {
      vueI18n: {
        fallbackLocale: 'en',
      },
      locales,
      lazy: true,
      langDir: 'i18n/',
      strategy: 'prefix_except_default',
      defaultLocale: 'en',
      detectBrowserLanguage: {
        useCookie: true,
        cookieKey: 'i18n_redirected',
        alwaysRedirect: false,
        fallbackLocale: 'en',
      },
    }],
  ],

  /*
  ** Nuxt.js modules
  */
  axios: {},

  sentry: {
    dsn: 'https://efc4e01857ad4237bf561950f6a02bbe@o268001.ingest.sentry.io/5316541',
    config: {
      environment: process.env.NODE_ENV,
    },
    disabled: process.env.NODE_ENV === 'development',
  },

  moment: {
    defaultLocale: 'en',
    locales: locales.map(({ code }) => code).filter((l) => l !== 'en'),
    timezone: true,
    defaultTimezone: 'America/Los_Angeles',
  },

  build: {
    extend({ module }) {
      const vueRule = module.rules.find((rule) => rule.test.test('.vue'));
      vueRule.use = [
        {
          loader: vueRule.loader,
          options: vueRule.options,
        },
        {
          loader: 'vue-svg-inline-loader',
          options: {
            removeAttributes: ['title'],
          },
        },
      ];
      delete vueRule.loader;
      delete vueRule.options;
    },
    postcss: {
      preset: {
        features: {
          customProperties: false,
        },
      },
    },
    loaders: {
      cssModules: {
        modules: {
          localIdentName: '[local]--[Frida]_[hash:base64:4]',
        },
      },
    },
  },
};
